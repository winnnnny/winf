package th.ac.tu.siit.lab7database;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class More extends Activity implements OnClickListener {
	
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.action_main:
				Intent c = new Intent(this, MainActivity.class);
				startActivityForResult(c, 8883);
				return true;
				
			}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_more);
		
		Button b1 = (Button)findViewById(R.id.btAbout);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.btWishList);
		b2.setOnClickListener(this);
		
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		
		if (id == R.id.btAbout) {
			
			Intent i = new Intent(this, About.class);
			startActivity(i);
		}
		else if (id == R.id.btWishList) {
			Intent i = new Intent(this, WishList.class);
			startActivity(i);
		}
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.more, menu);
		return true;
	}
	
	

}
