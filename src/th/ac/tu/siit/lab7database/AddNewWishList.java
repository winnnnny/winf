package th.ac.tu.siit.lab7database;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class AddNewWishList extends Activity {

	long id;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_new_wish_list);
		Intent i = this.getIntent();
		if (i.hasExtra("id")){
			((EditText)findViewById(R.id.etItem)).setText(i.getStringExtra("ct_Item"));
			((EditText)findViewById(R.id.etPrice)).setText(i.getStringExtra("ct_Price"));
			((EditText)findViewById(R.id.etAmount1)).setText(i.getStringExtra("ct_Amount1"));
			((EditText)findViewById(R.id.etWhereToBuy)).setText(i.getStringExtra("ct_WhereToBuy"));
			
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_new_wish_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_savewish:
			EditText etItem = (EditText)findViewById(R.id.etItem);
			EditText etPrice = (EditText)findViewById(R.id.etPrice);
			EditText etAmount1 = (EditText)findViewById(R.id.etAmount1);
			EditText etWhereToBuy = (EditText)findViewById(R.id.etWhereToBuy);
			
			String sItem = etItem.getText().toString().trim();
			String sPrice = etPrice.getText().toString().trim();
			String sAmount1 = etAmount1.getText().toString().trim();
			String sWhereToBuy = etWhereToBuy.getText().toString().trim();
			
			if (sItem.length() == 0 || sPrice.length() == 0 || sAmount1.length() == 0) {
				Toast t = Toast.makeText(this, 
						"Item name,  price, and amount are all required", 
						Toast.LENGTH_LONG);
				t.show();
			}
			else {
				Intent data = new Intent();
				data.putExtra("Item", sItem);
				data.putExtra("Price", sPrice);
				data.putExtra("Amount1", sAmount1);
				data.putExtra("WhereToBuy", sWhereToBuy);
				data.putExtra("id", id);
				
				setResult(RESULT_OK, data);
				finish();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}}

