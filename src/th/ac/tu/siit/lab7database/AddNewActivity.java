package th.ac.tu.siit.lab7database;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class AddNewActivity extends Activity {
	long id;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_new);
		Intent i = this.getIntent();
		if (i.hasExtra("id")){
			((EditText)findViewById(R.id.etSubject)).setText(i.getStringExtra("ct_Subject"));
			((EditText)findViewById(R.id.etAmount)).setText(i.getStringExtra("ct_Amount"));
			((EditText)findViewById(R.id.etDate)).setText(i.getStringExtra("ct_Date"));
			RadioGroup rdg = (RadioGroup)findViewById(R.id.rdgType);
			int itype = i.getIntExtra("ct_type", -1);
			if(itype == R.drawable.home)
				rdg.check(R.id.rdIncomes);
			else
				rdg.check(R.id.rdOutcomes);
			id = i.getLongExtra("id" ,-1);
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_new, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_save:
			EditText etSubject = (EditText)findViewById(R.id.etSubject);
			EditText etAmount = (EditText)findViewById(R.id.etAmount);
			EditText etDate = (EditText)findViewById(R.id.etDate);
			RadioGroup rdg = (RadioGroup)findViewById(R.id.rdgType);
			
			String sSubject = etSubject.getText().toString().trim();
			String sAmount = etAmount.getText().toString().trim();
			String sDate = etDate.getText().toString().trim();
			int iType = rdg.getCheckedRadioButtonId();
			
			if (sSubject.length() == 0 || sAmount.length() == 0 || iType == -1) {
				Toast t = Toast.makeText(this, 
						"Contact name,  phone, and type are required", 
						Toast.LENGTH_LONG);
				t.show();
			}
			else {
				Intent data = new Intent();
				data.putExtra("Subject", sSubject);
				data.putExtra("Amount", sAmount);
				data.putExtra("Date", sDate);
				data.putExtra("id", id);
				switch(iType) {
				case R.id.rdIncomes:
					data.putExtra("type", String.valueOf(R.drawable.home));
					break;
				case R.id.rdOutcomes:
					data.putExtra("type", String.valueOf(R.drawable.mobile));
					break;
				}
				setResult(RESULT_OK, data);
				finish();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
}
