package th.ac.tu.siit.lab7database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper2 extends SQLiteOpenHelper {

	private static final String DBNAME = "WishList.db";
	private static final int DBVERSION = 1;
	
	public DBHelper2(Context mtx, String name, int version) {
		super(mtx, DBNAME, null, DBVERSION);
	}
	
	//Internal storage.
	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql2 = "CREATE TABLE WishListTable1 (" +
				"_id integer primary key autoincrement, " +
				"ct_Item text not null, " +
				"ct_Price integer not null, " +
				"ct_Amount1 integer not null, " +
				"ct_WhereToBuy text not null);";
		db.execSQL(sql2);	
		//The primary key of the table needs to be "_id" when we want to
		//display the data in ListView.
	}

	//Called when the database exists, but the DBVERSION is higher than the version
	//in the file.
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String sql2 = "DROP TABLE IF EXISTS WishListTable1;";
		db.execSQL(sql2);
		this.onCreate(db);
		//This is not recommended in practice because all records in the table will
		//be removed. We should use "ALTER TABLE" instead.
	}


}
