package th.ac.tu.siit.lab7database;

import android.os.Bundle;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class WishList extends ListActivity {
	
	
	
	DBHelper2 dbHelper2;
	SQLiteDatabase db;
	Cursor cursor; //for managing the records retrieved from the table
				   //it works like List<Map<String,String>> in the previous lab.
	SimpleCursorAdapter adapter; //Use Cursor as the data source
	
	


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_WishListToMain:
			Intent b = new Intent(this, MainActivity.class);
			startActivityForResult(b, 8884);
			return true;
		case R.id.action_AddWish:
			Intent i = new Intent(this, AddNewWishList.class);
			startActivityForResult(i, 9998);
			return true;
			
		}
	return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dbHelper2 = new DBHelper2(this, null, 0);
		db = dbHelper2.getWritableDatabase(); //We can update the database
		cursor = getAllWishList1();
		adapter = new SimpleCursorAdapter(this, R.layout.item, cursor, 
				new String[] {"ct_Item", "ct_Price", "ct_Amount1", "ct_WhereToBuy"},
				new int[] {R.id.tvItem, R.id.tvPrice, R.id.tvAmount1, R.id.tvWhereToBuy}, 0);
		setListAdapter(adapter);
		registerForContextMenu(getListView());
	}
	private Cursor getAllWishList1() {
		//db.query executes "SELECT" statement.
		return db.query("WishListTable1", //table name
				new String[] {"_id", "ct_Item", "ct_Price",
				"ct_Amount1", "ct_WhereToBuy"}, //list of columns
				null, //condition for WHERE clause e.g. "ct_name LIKE ?"
				null, //values for the conditions e.g. new String[] ( "John" )
				null, //GROUP BY
				null, //HAVING
				"_id asc"); //ORDER BY
		//SELECT _id, ct_name, ct_phone, ct_type, ct_email, FROM contacts
		//ORDER BY ct_name ASC;
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.wish_list, menu);
		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.context, menu);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		//Close everthing before letting the application ends
		cursor.close();
		db.close();
		dbHelper2.close();
		finish();
	}

	@Override
	protected void onActivityResult(int requestCode, 
			int resultCode, Intent data) {
		if (requestCode == 9998 && resultCode == RESULT_OK) {
			//Insert a new record to the table
			//ContentValues is a class of storing key-value pairs for the table.
			ContentValues v = new ContentValues();
			v.put("ct_Item", data.getStringExtra("Item"));
			v.put("ct_Price", data.getStringExtra("Price"));
			v.put("ct_Amount1", data.getStringExtra("Amount1"));
			v.put("ct_WhereToBuy", data.getStringExtra("WhereToBuy"));
			db.insert("WishListTable1", null, v);
			//Refresh the ListView in order to display the new record
			//(1) Re-retrieve all the records
			cursor = getAllWishList1();
			//(2) Update the cursor in the adapter
			adapter.changeCursor(cursor);
			//(3) Notify that the data have changed
			adapter.notifyDataSetChanged();
		}
		//else if (requestCode == 9999 && resultCode == RESULT_OK) {
			//Insert a new record to the table
			//ContentValues is a class of storing key-value pairs for the table.
			//ContentValues v = new ContentValues();
			//long id = data.getLongExtra("id", -1);
			//v.put("ct_Item", data.getStringExtra("Item"));
			//v.put("ct_Price", data.getStringExtra("Price"));
			//v.put("ct_Amount1", data.getStringExtra("Amount1"));
			//v.put("ct_WhereToBuy", data.getStringExtra("WhereToBuy"));
			//String selection = "_id = ?";
			//String[] selectionArgs = { String.valueOf(id) };
			//db.update("WishListTable1", v, selection, selectionArgs);
			/////////Refresh the ListView in order to display the new record
			////////(1) Re-retrieve all the records
			//cursor = getAllWishList1();
			////////(2) Update the cursor in the adapter
			//adapter.changeCursor(cursor);
			///////(3) Notify that the data have changed
			//adapter.notifyDataSetChanged();
		//}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo a = (AdapterContextMenuInfo)item.getMenuInfo();
		long id = a.id; //id of the selected item
		int position = a.position; //position of the selected item
		
		switch(item.getItemId()) {
		case R.id.action_edit:
			//Get the selected record
			Cursor c = (Cursor)adapter.getItem(position);
			//Get the value of "ct_name" column
			String Item = c.getString(c.getColumnIndex("ct_Item"));
			String Price = c.getString(c.getColumnIndex("ct_Price"));
			String Amount1 = c.getString(c.getColumnIndex("ct_Amount1"));
			String WhereToBuy = c.getString(c.getColumnIndex("ct_WhereToBuy"));
			
			//Prepare an Intent
			Intent i = new Intent(this, AddNewWishList.class);
			//Attach the data to the intent
			i.putExtra("ct_Item", Item);
			i.putExtra("ct_Price", Price);
			i.putExtra("ct_Amount1", Amount1);
			i.putExtra("ct_WhereToBuy", WhereToBuy);
			i.putExtra("id", id); //long -> getLongExtra
			//Start AddNEwActivity and wait for the result
			startActivityForResult(i, 8888);
			Toast t = Toast.makeText(this, "Selected ID = "+id+
					" with name = "+Item, Toast.LENGTH_LONG);
			t.show();
			return true;
		case R.id.action_delete:
			String selection = "_id = ?";
			String[] selectionArgs = { String.valueOf(id) };
			db.delete("WishListTable1", selection, selectionArgs);
			cursor = getAllWishList1();
			//(2) Update the cursor in the adapter
			adapter.changeCursor(cursor);
			//(3) Notify that the data have changed
			adapter.notifyDataSetChanged();
			
			return true;
		}
		return super.onContextItemSelected(item);
	}
	
	
	
	
	

}
